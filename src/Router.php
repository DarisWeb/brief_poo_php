<?php

include_once(__DIR__ . '/../src/config.php');
include_once(__DIR__ . '/../src/functions.php');

class Router{
    private function doGet($routes, $dbConnection)
    {
        $page = getParam('p');
        $action = getParam('a');

        if ($page === NULL)
        {
            $page = DEFAULT_PAGE;
        }

        if ($action === NULL)
        {
            $action = DEFAULT_ACTION;
        }
        
        if (array_key_exists($page, $routes)) {
            $matched = false;
            //var_dump($routes);
            foreach($routes[$page] as $value)
            {
                if ($action === $value)
                {
                    
                    $matched = true;
                    break;                
                } 
            }

            if ($matched)
            {
                //$controller = null;

                $controllerName = $page; // home
                $controllerName = ucfirst($controllerName); // Home
                $controllerName .= 'Controller'; // HomeController


                $controller = new $controllerName();

                $controller->$action($dbConnection);
                /*
                switch ($page) {
                    case 'home':
                        $controller = new HomeController();
                        break;
                    case 'student':
                        $controller = new StudentController();
                }

                switch ($action) {
                    case 'index':
                        $controller->index();
                        break;
                    case 'add':
                        $controller->add();
                        break;
                }

                $controller->$action($dbConnection);

                /*
                $pageFile = PAGE . $page . '.php';
                if (file_exists($pageFile))
                {
                    include_once($pageFile);
                    $action($dbConnection);
                }
                */
            } else {
                echo 'Error 404<br>Page not found';
            }
            
        } else {
            echo 'Error 404<br>Page not found';
        }
    }

    private function doPost($routes, $dbConnection)
    {
        
        $page = getParam('p');
        $action = getParam('a');

        if ($page === NULL)
        {
            $page = DEFAULT_PAGE;
        }

        if ($action === NULL)
        {
            $action = DEFAULT_ACTION;
        }
        
        if (array_key_exists($page, $routes)) {
            $matched = false;
            foreach($routes[$page] as $value)
            {
                if ($action === $value)
                {
                    
                    $matched = true;
                    break;                
                } 
            }

            if ($matched)
            {
                $controllerName = $page; // home
                $controllerName = ucfirst($controllerName); // Home
                $controllerName .= 'Controller'; // HomeController


                $controller = new $controllerName();

                $controller->$action($dbConnection);
                /*
                $pageFile = PAGE . $page . '.php';
                if (file_exists($pageFile))
                {
                    include_once($pageFile);
                    $action($dbConnection);
                }
                */
            } else {
                echo 'Error 404<br>Page not found';
            }
            
        } else {
            echo 'Error 404<br>Page not found';
        }
    }

    public function init($routes, $dbConnection)
    {
        $httpMethod = httpMethod();
        
        switch ($httpMethod) {
            case 'GET':
                $this->doGet($routes, $dbConnection);
                break;

            case 'POST':
                $this->doPost($routes, $dbConnection);
                break;

            default:
                break;
        }
    }
}
