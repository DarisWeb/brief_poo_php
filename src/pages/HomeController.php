<?php
define('HOME', 'home/');

class HomeController {
    
    public function index()
    {
        
        $pageName = 'home';

        include_once(TEMPLATES . HOME . 'index.php');
    }
}