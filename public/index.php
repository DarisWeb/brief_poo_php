<?php
include_once(__DIR__ . '/../src/config.php');
include_once(__DIR__ . '/../src/functions.php');
include_once(__DIR__ . '/../src/Router.php');
include_once(__DIR__ . '/../src/pages/HomeController.php');
include_once(__DIR__ . '/../src/pages/StudentController.php');

$dbConnection = getDbConnection();

$router = new Router;

$router->init($routes, $dbConnection);